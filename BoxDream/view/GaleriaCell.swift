//
//  galeria.swift
//  BoxDream
//
//  Created by Admin on 16/03/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit
import SDWebImage

class GaleriaCell: BaseViewCell {
    var eventDetailDelegate : EventDetailViewController?
    var image : String?{
        didSet {
            if let img = image{
                
                imageView.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "placeholder"))
            }
        }
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "placeholder")
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.backgroundColor = .red
        return iv
    }()
    
    
    override func setupViews() {
        super.setupViews()
        backgroundColor = .clear
        addSubview(imageView)
        imageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0 , leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
    
  

}
