//
//  BaseViewCell.swift
//  BoxDream
//
//  Created by Thiago Santos on 07/10/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit

class BaseViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
        
    }
    
    
    
    func setupViews() {
        
    }
}
