//
//  MenuCell.swift
//  BoxDream
//
//  Created by Admin on 02/11/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit

class MenuCell: BaseViewCell {
    
    let titleLabel : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.text = "teste"
        label.font = UIFont(name: "OpenSans-ExtraBold", size: 12)
        return label
    }()
    
   override func setupViews(){
    addSubview(titleLabel)
    titleLabel.anchor(self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        backgroundColor =  UIColor.rgb(red: 192, green: 23, blue: 73)
    }

}
