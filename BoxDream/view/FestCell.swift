//
//  FestCell.swift
//  BoxDream
//
//  Created by Admin on 02/11/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import SwiftMoment


class FestCell: BaseViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate{
    let servcostId = "servcostId"
    let serviceId = "teste"
    var eventDetailDelegate : EventDetailViewController?
    var serviceView = [ServiceView]()
    var count = 2
    
    
    var event : Event?{
        didSet {
            
            if let urlImage = event?.imageDestaque{
                let url = URL(string: urlImage)
                imageView.sd_setImage(with: url, placeholderImage:UIImage(named: "placeholder"))
            }
            if let titulo = event?.name{
                titleLabel.text = titulo.uppercased()
            }
            if let descri = event?.descriptions{
                descricaoLabel.text = descri
            }
            if let dateInicio = event?.dateInicio{
                
                dateLabel.text = dateInicio
            }
            if let count  = event?.servicos.count{
                self.count = count
                guard let  services = event?.servicos else { return }
                for serviceArray in services {
                    var  service = ServiceView()
                    
                    service.titleLabel.text         = serviceArray.name
                    service.descriptionLabel.text   = serviceArray.descript
                    service.imageView.image         = UIImage(named: serviceArray.icone!)
                    serviceView.append(service)
                
                }
               //  collectionViewServicos.reloadData()
                
            }
            
            
           
        }
        
    }
    
    lazy var scrollView : UIScrollView = {
        
        let scroll = UIScrollView()
        scroll.isScrollEnabled = true
        scroll.delegate = self
        scroll.alwaysBounceVertical = true
   
        return scroll
    }()
    let imageView: UIImageView = {
       let iv = UIImageView()
        iv.image = UIImage(named: "placeholder")
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    let backgroundimageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "bg")
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    let calendImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "calendar-blank")
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Teste"
        label.font = UIFont(name: "OpenSans-ExtraBold", size: 20)
        label.textAlignment = .center
        return label
    }()
    lazy var collectionViewServicos: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.isPagingEnabled = true
        cv.dataSource = self
        cv.delegate = self
      
       
        return cv
    }()
    
    lazy var collectionViewService: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.dataSource = self
        cv.delegate = self
        
        
        return cv
    }()
    //let uiButton: UIButton = {
      //  let bt = UIButton()
       // bt.backgroundColor =  UIColor.rgb(red: 250, green: 227, blue: 186)
        //bt.setTitle("Comprar Ingresso", for: .normal)
        //bt.setTitleColor(.white, for: .normal)
       // bt.layer.cornerRadius = 5
     //   /return bt
    //}()
    //dateInicio	String	"2016-09-15 12:57:04"
    let descricaoLabel: UILabel = {
        let label = UILabel()
        label.text = "Teste"
        label.font = UIFont(name: "OpenSans-Light", size: 12)
        label.numberOfLines = 0
        return label
    }()
    let dateLabel: UILabel = {
        let label = UILabel()
        label.text = "Teste"
        label.font = UIFont(name: "OpenSans-Light", size: 12)
        label.numberOfLines = 0
        label.textColor = UIColor.lightGray
        return label
    }()
    let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.pageIndicatorTintColor = .lightGray
        pc.currentPageIndicatorTintColor = UIColor.rgb(red: 192, green: 23, blue: 73)
      
       return pc
    }()
    
    func animate() {
        
        
    }
    override func setupViews() {
        super.setupViews()
        collectionViewServicos.register(GaleriaCell.self, forCellWithReuseIdentifier: servcostId)
        collectionViewService.register(Service.self, forCellWithReuseIdentifier: serviceId)
       
    
        addSubview(backgroundimageView)
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(descricaoLabel)
        addSubview(calendImageView)
        addSubview(dateLabel)
        
        addSubview(collectionViewServicos)
        
        addSubview(pageControl)
        
        backgroundColor = .white
        
        
    
        backgroundimageView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        imageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 10 , leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 150)
        titleLabel.anchor(imageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        descricaoLabel.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        calendImageView.anchor(descricaoLabel.bottomAnchor, left: descricaoLabel.leftAnchor, bottom: nil, right: self.dateLabel.leftAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 20)
        dateLabel.anchor(calendImageView.topAnchor, left: calendImageView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        

        
        if self.serviceView.count > 0{
            for index  in 0 ..< self.count {
                
                
                addSubview(serviceView[index])
                if index == 0{
                    serviceView[index].anchor(calendImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
                }else {
                    serviceView[index].anchor(serviceView[index - 1].bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
                }
        }
        }else{
            collectionViewServicos.anchor(calendImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)
        }

        
        pageControl.anchor(collectionViewServicos.bottomAnchor, left: descricaoLabel.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
 

    }
}

extension FestCell{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewServicos{
            if let count = event?.galeria.count{
                pageControl.numberOfPages = count
                return count
            }
        }else{
            if let count = event?.servicos.count{
                return count
            }
        }
        
        
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       if collectionView == collectionViewServicos{
            return CGSize(width: frame.width , height: 150)
       }else{
            return CGSize(width: frame.width , height: 150)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewServicos{
            let cell =  collectionViewServicos.dequeueReusableCell(withReuseIdentifier: servcostId, for: indexPath) as! GaleriaCell
            cell.image = event?.galeria[indexPath.item]
            cell.eventDetailDelegate = eventDetailDelegate
        
            return cell
        }else{
        let cell =  collectionViewService.dequeueReusableCell(withReuseIdentifier: serviceId, for: indexPath) as! Service
            cell.service = event?.servicos[indexPath.item]
            return cell
        }
    }
   
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber  = Int(targetContentOffset.pointee.x / frame.width )
        pageControl.currentPage = pageNumber
       
        
    }
}
