//
//  ServiceView.swift
//  BoxDream
//
//  Created by Thiago Santos on 21/04/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class ServiceView: UIView {
    
    
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "martini")
        iv.contentMode = .scaleAspectFill
        
        iv.clipsToBounds = true
        return iv
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Teste"
        label.font = UIFont(name: "OpenSans-ExtraBold", size: 12)
        label.textAlignment = .left
        
        return label
    }()
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Teste"
        label.font = UIFont(name: "OpenSans-Regular", size: 10)
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        
        imageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
        titleLabel.anchor(topAnchor, left: imageView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        descriptionLabel.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
