//
//  EventsViewCell.swift
//  BoxDream
//
//  Created by Thiago Santos on 07/10/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import SDWebImage

class EventsViewCell: BaseViewCell {
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var imageDestaqueImageView: UIImageView!
    var event : Event?{
        didSet {
            if let urlImage = event?.imageDestaque{
                
                let url = URL(string: urlImage)
                imageDestaqueImageView.sd_setImage(with: url, placeholderImage:UIImage(named: "placeholder"))
                imageDestaqueImageView.alpha = 0.5
            }
            if let title = event?.name{
                titleLabel.text = title
                
            }
            
        }
    }
    
    override func setupViews() {
        backgroundColor = UIColor.white
        
        imageDestaqueImageView?.layer.cornerRadius = 5
        imageDestaqueImageView?.contentMode = .scaleAspectFill
        imageDestaqueImageView?.clipsToBounds = true
    }

}
