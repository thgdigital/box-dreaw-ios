//
//  Service.swift
//  BoxDream
//
//  Created by Thiago Santos on 21/03/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class Service: BaseViewCell {
    var service : Servicos?{
        didSet {
            if let icone = service?.icone{
            imageView.image = UIImage(named: icone)
            }
            if let name = service?.name{
                titleLabel.text = name
            }
            if let descript = service?.descript{
                descriptionLabel.text = descript
            }
        }
    }

    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "martini")
        iv.contentMode = .scaleAspectFill
       
        iv.clipsToBounds = true
        return iv
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Teste"
        label.font = UIFont(name: "OpenSans-ExtraBold", size: 12)
        label.textAlignment = .left
        
        return label
    }()
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Teste"
        label.font = UIFont(name: "OpenSans-Regular", size: 10)
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }()
    
    override func setupViews() {
        super.setupViews()
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        
        imageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
        titleLabel.anchor(topAnchor, left: imageView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        descriptionLabel.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
   
}
