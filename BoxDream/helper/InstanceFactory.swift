//
//  InstanceFactory.swift
//  BoxDream
//
//  Created by Thiago Santos on 31/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

struct InstanceFactory {
    static let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
}
