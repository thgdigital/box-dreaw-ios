//
//  ViewController.swift
//  BoxDream
//
//  Created by Thiago Santos on 30/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        if self.emailTextField.text == "" || self.passwordTextField.text == ""{
            let alert = alertController(with: "Existe dados em brancos")
            self.present(alert, animated: true, completion: nil)
            
        }else if !Utils.isValidEmail(email: self.emailTextField.text!){
            let alert = alertController(with: "Preencher um e-mail valido")
            self.present(alert, animated: true, completion: nil)
        }else{
            
            
            Auth.auth().signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
                
                if error == nil {
                    
                    //Print into the console if successfully logged in
                    print("You have successfully logged in")
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = TabCustomController()
                    
                } else {
                    
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = self.alertController(with: (error?.localizedDescription)!)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
        }
        self.view.endEditing(true);
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true);
    }
    override var shouldAutorotate: Bool {
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
extension LoginViewController : UITextFieldDelegate{
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }
}
