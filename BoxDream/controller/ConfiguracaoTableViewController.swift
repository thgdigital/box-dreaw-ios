//
//  ConfiguracaoTableViewController.swift
//  BoxDream
//
//  Created by Thiago Santos on 31/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class ConfiguracaoViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }

    // MARK: - Instance
    
    class func instantiateFromStoryboard() -> ConfiguracaoViewController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "ConfiguracaoViewController") as! ConfiguracaoViewController
    }
    func setupLayout(){
        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        
        editButton.layer.borderColor = UIColor.gray.cgColor
        editButton.layer.borderWidth = 0.5
        editButton.layer.cornerRadius = 5
        

        
    }
}
