//
//  TabCustomControllerTableViewController.swift
//  BoxDream
//
//  Created by Thiago Santos on 20/03/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class TabCustomController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Setup
    func setup(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeControler = storyboard.instantiateViewController(withIdentifier: "EventViewController") as! EventViewController
        let navigaitorHome = UINavigationController(rootViewController: homeControler)
        homeControler.tabBarItem.title = "Home"
        homeControler.tabBarItem.image = UIImage(named: "home")
        homeControler.tabBarItem.selectedImage = UIImage(named: "home_selected")
        
    
        
        let ingressos = UINavigationController(rootViewController: UIViewController())
        ingressos.tabBarItem.image = UIImage(named: "ticket-account")
        ingressos.view.backgroundColor = .white
        ingressos.navigationController?.title = "Ingressos"
        ingressos.tabBarItem.title = "Ingressos"
        
        let servicos = UINavigationController(rootViewController: UIViewController())
        servicos.tabBarItem.image = UIImage(named: "services")
        servicos.view.backgroundColor = .white
        servicos.navigationController?.title = "Serviços"
        servicos.tabBarItem.title = "Serviços"
   
    
        let friend = UINavigationController(rootViewController: MessageViewController.instantiateFromStoryboard())
        friend.tabBarItem.image = UIImage(named: "account-multiple")
        friend.view.backgroundColor = .white
        friend.navigationController?.title = "Conecte-se"
        friend.tabBarItem.title = "Conecte-se"
     
        let configuracao = ConfiguracaoViewController.instantiateFromStoryboard()
        let config = UINavigationController(rootViewController: configuracao)
        config.tabBarItem.image = UIImage(named: "menu")
        config.view.backgroundColor = .white
        config.tabBarItem.title = "Configuração"
        config.navigationController?.title = "Configuração"
    
        self.tabBar.isTranslucent = false
     
        
        self.viewControllers = [navigaitorHome, ingressos, servicos, friend, config]

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
