//
//  MessageViewController.swift
//  BoxDream
//
//  Created by Thiago Santos on 31/05/17.
//  Copyright © 2017 Thiago Santos. All rights reserved.
//

import UIKit

class MessageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Conecte-se"
        setupLayout()
    }


    // MARK: - Instance
    
    class func instantiateFromStoryboard() -> MessageViewController {
        return InstanceFactory.storyboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
    }
    
    func setupLayout(){
        let button = UIButton(type: .system)
        let image = UIImage(named: "heart")?.maskWithColor(color: .white)
        button.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    
    }

}
