//
//  EventViewController.swift
//  BoxDream
//
//  Created by Thiago Santos on 07/10/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit

class EventViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate{
    private let cellId = "cellId"
    let footerReuseIdentifier: String = "footer"
    
    var events = [Event]()
    
    // MARK: - Loader da Paginação
    let activityView = UIActivityIndicatorView()
    
    private var nextPageURL: String?
    private var loadingItens: Bool = false {
        didSet {
            self.collectionViewLayout.invalidateLayout()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        self.collectionView!.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter , withReuseIdentifier: footerReuseIdentifier)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.rgb(red: 192, green: 23, blue: 73)
        self.navigationController?.navigationBar.tintColor = .white
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        collectionView?.contentInset = UIEdgeInsetsMake(3, 0, 0, 0)
        collectionView?.scrollIndicatorInsets  = UIEdgeInsetsMake(3, 0, 0, 0)
        navigationItem.title = "BOX DREAW"
        activityView.color = UIColor.black
        collectionView?.backgroundColor = .white
//        ApiSearch.sharedInstance.getEvents(urlString: "http://private-79d462-boxdream.apiary-mock.com/eventos") { (result,next, status) in
//            if status {
//                self.events = result
//                self.nextPageURL = next
//                self.collectionView?.reloadData()
//            }
//        }
        ApiSearch.sharedInstance.getEventos(urlString: "http://private-79d462-boxdream.apiary-mock.com/eventos") { (result, status) in
            if status {
                self.events = result
                self.collectionView?.reloadData()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
    }
    // MARK: - UIScrollViewDelegate
    
    
    // MARK: - scrollViewDidScroll
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        // Request
//        if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.bounds.height {
//            self.fetchMoreItemsIfNeeded()
//        }
        
    }
    // MARK: - FooterCollection
     override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let footerView: UICollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerReuseIdentifier, for: indexPath )
        
        if self.loadingItens && (self.nextPageURL != nil || self.events.count == 0) {
            footerView.clipsToBounds = true
            self.activityView.frame.origin.x = (footerView.frame.width / 2) - self.activityView.frame.width/2
            self.activityView.frame.origin.y = footerView.frame.height / 2 - self.activityView.frame.height/2
            
            footerView.addSubview(self.activityView)
            self.activityView.startAnimating()
        } else {
            self.activityView.removeFromSuperview()
            
        }
        
        return footerView
    }
    
    
    private func fetchMoreItemsIfNeeded() {
        // Tem próxima página
        guard let next =  self.nextPageURL , !self.loadingItens else {
            //TODO: Validar falta de conectividade
            return
        }
            self.loadingItens = true
            let nextPage = next
        
            ApiSearch.sharedInstance.getEventFromPage(urlString: nextPage, completionHandler: { (result, nextPage, status) in
                self.loadingItens = false
                
                if status == true {
                    self.nextPageURL = nextPage
                    self.events.append(contentsOf: result)
                    self.collectionView?.reloadData()
                }
                
            })
        
    }
    // MARK: - showAppDetailForApp
    func showAppDetailForApp(event: Event) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let singleProdutoController = EventDetailViewController(collectionViewLayout: layout)
        singleProdutoController.event = event
        navigationController?.pushViewController(singleProdutoController, animated: true)
    }
    
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 130)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return events.count
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      showAppDetailForApp(event: events[indexPath.item])
      
    }
    
    
   override  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! EventsViewCell
        
      cell.event = events[indexPath.item]
        
        return cell
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if self.loadingItens && (self.nextPageURL != nil || self.events.count == 0)  {
            return CGSize(width: view.frame.width, height: 40);
        }
        return CGSize(width: view.frame.width, height:0);
    }
}
