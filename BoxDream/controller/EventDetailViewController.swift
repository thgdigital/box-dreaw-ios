//
//  EventDetailViewController.swift
//  BoxDream
//
//  Created by Thiago Santos on 07/10/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import SDWebImage
import Viewer

class EventDetailViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate {
   

    var event  = Event()
    let festId = "festId"
    let cellId = "cellId"
   

    override func viewDidLoad() {
        super.viewDidLoad()

        leftBar()
        self.navigationController?.navigationBar.barTintColor = UIColor.rgb(red: 192, green: 23, blue: 73)
       
        collectionView?.register(FestCell.self, forCellWithReuseIdentifier: festId)
        
        collectionView?.autoresizingMask = [.flexibleWidth,  .flexibleHeight]

        if let name = event.name{

            navigationItem.title = name

        }
        self.navigationItem.hidesBackButton = true

        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
        
            layout.minimumLineSpacing = 0
            
        }
   
     
        self.collectionView?.isScrollEnabled = true
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
    
    
    let zoomImageView = UIImageView()
    let blackBackgroungView = UIView()
    var imageView = UIImageView()
    let navBarBack = UIView()
    let tabBarBack = UIView()
    var index = IndexPath()
    
    
    
    func animateImageView(_ index: Int, indexPath: IndexPath, collectionView: UICollectionView){
        self.index = indexPath
        imageView.sd_setImage(with: URL(string: event.galeria[index]) , placeholderImage:UIImage(named: "placeholder"))
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    
    
    
    
    lazy var  menuBar: MenuBar = {
        let mb = MenuBar()
        mb.eventDetailController = self
        mb.invalidateIntrinsicContentSize()
     
        return mb
    }()

    private func apiService(){
        guard let id = event.id else {
            print("error")
            return
        }
        
        ApiSearch.sharedInstance.byIdEvent(id:id) { (event: (Event), error) in
            if error != nil{
                print("error ao buscar event")
                return
            }
           
        }
    
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let index = targetContentOffset.pointee.x / view.frame.width
        
        let indexPath = IndexPath(item: Int(index), section: 0)
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)

    }

    private func setupMenuBar(){
        view.addSubview(menuBar)
        menuBar.anchor(self.view.topAnchor, left: self.view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
    }


    
}
extension EventDetailViewController {
    
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        menuBar.horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / 4
//        
//    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
   
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
      
        
        collectionView?.collectionViewLayout.invalidateLayout()
    }
 
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: festId, for: indexPath) as! FestCell
        cell.eventDetailDelegate = self
        cell.event = event
        
        return cell
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        
        let approximateWidthOfDescription = view.frame.width - 12 - 50 - 8
        let size = CGSize(width: approximateWidthOfDescription, height: 1000)
        let atrribute = [NSFontAttributeName: UIFont(name: "OpenSans-Light", size: 12)]
        let estimatedFrame =  NSString(string: event.descriptions!).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: atrribute , context: nil)
        
        return CGSize(width: view.frame.width, height: view.frame.height + estimatedFrame.height + 30)
    }

    public func numberOfItemsInViewerController(_ viewerController: ViewerController) -> Int {
       
        return event.galeria.count
    }
    override func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return collectionView
    }
    
}
