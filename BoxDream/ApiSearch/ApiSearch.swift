//
//  ApiSearch.swift
//  BoxDream
//
//  Created by Thiago Santos on 07/10/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiSearch {
    
    static let sharedInstance = ApiSearch()
    func getEvents(urlString: String, completionHandler: @escaping (_ result: [(Event)], _ nextPage: String? ,  _ status: Bool) -> ()){
       
            Alamofire.request(urlString)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                       var  events = [Event]()
                        if let jsonObject = response.result.value {
                            let json = JSON(jsonObject)
                            //let next = json["next_page_url"].stringValue
                           

                            for (_, subJson):(String, JSON) in json{
                                let event = Event(json: subJson)
                                events.append(event)
                                
                            }
                            let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                            DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                                completionHandler(events, nil,  true)
                            })
                        }
                    
                       
                    case .failure(let error):
                        print(error)
                        completionHandler([], nil, false)
                    }

                }
    
        }
    func getEventos(urlString: String, completionHandler: @escaping (_ result: ([Event]), _ status: Bool) -> ()) {
        Alamofire.request(urlString)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    var  events = [Event]()
                    if let jsonObject = response.result.value {
                        let json = JSON(jsonObject)
                       for (_, subJson):(String, JSON) in json{
                        let event = Event(json: subJson)
                            events.append(event)
                        }
                        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                            completionHandler(events, true)
                        })
                    }
                    
                    
                case .failure(let error):
                    print(error)
                    completionHandler([], false)
                }
                
        }
        
    }
    
     func getEventFromPage(urlString: String , completionHandler: @escaping (_ result: ([Event]), _ nextPage: String?, _ status: Bool) -> ()) {
        getEvents(urlString: urlString, completionHandler: completionHandler)
    }
    
    
    func byIdEvent(id: NSNumber ,  completionHandler: @escaping (_ result: (Event), _  error: Error?) -> ()){
        let url = "http://localhost:8000/api/events/\(id)/ver"
        Alamofire.request(url)
            .validate()
            .responseJSON { response in
                switch response.result {
                    
                case .success:
                    if let jsonObject = response.result.value {
                        let json        = JSON(jsonObject)
                        let event = Event(json: json)
                        
                        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                            completionHandler(event, nil)
                        })
                    }
                case .failure(let error):
                    let event = Event(json: nil)
                    completionHandler(event, error)
                }
        }
    }
}
