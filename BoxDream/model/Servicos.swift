//
//  Servicos.swift
//  BoxDream
//
//  Created by Thiago Santos on 01/11/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import SwiftyJSON

class Servicos: NSObject {
    
    var icone: String?
    var name: String?
    var imageDestaque: String?
    var descript:String?
    var created_at: String?
    var updated_at: String?
    
    
    init(json: JSON?) {
        
        self.icone           = json?["icone"].stringValue
        self.name            = json?["name"].stringValue
        self.descript        = json?["servicos"].stringValue
      
    }
}
