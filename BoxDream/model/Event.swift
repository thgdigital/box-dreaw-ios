//
//  Models.swift
//  BoxDream
//
//  Created by Thiago Santos on 03/10/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import SwiftyJSON

class Event: NSObject {
    var id: NSNumber?
    var name: String?
    var imageDestaque: String?
    var descriptions:String?
    var dateInicio: String?
    var dateFim: String?
    var servicos = [Servicos]()
    var galeria = [String]()
    
    init(json: JSON?) {
        
        id                = json?["id"].numberValue
        name              = json?["name"].stringValue
        descriptions      = json?["description"].stringValue
        imageDestaque     = json?["image_destaque"].stringValue
        dateInicio        = json?["date_inicio"].stringValue
        dateFim           = json?["date_fim"].stringValue
        servicos          = [Servicos]()
      
        guard let servicoDictionary = json?["servicos"].arrayValue else {
            print("não existem array de serviços")
            return
        }
         servicos = servicoDictionary.map({ return Servicos(json: $0) })
        guard let galerias = json?["galeria"].arrayValue else {
            print("não existem array de galeria")
            return
        }
        galeria = galerias.map({return $0.stringValue})
        
    
    }
   override init(){}
    
}


