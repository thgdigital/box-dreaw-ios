//
//  CreateAccount.swift
//  BoxDream
//
//  Created by Thiago Santos on 30/09/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth



class CreateAccountViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    var imageBool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //handleSelectProfileImage
        nameTextField.delegate = self
        passwordTextfield.delegate = self
        emailTextField.delegate = self
        avatarImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImage)))
        
    }
    

    @IBAction func createAccount(_ sender: Any) {
        
        if self.emailTextField.text == "" || self.passwordTextfield.text == "" || self.nameTextField.text ==  ""{
            let alert = alertController(with: "Existe dados em brancos")
            self.present(alert, animated: true, completion: nil)
            
        }else if !Utils.isValidEmail(email: self.emailTextField.text!){
            let alert = alertController(with: "Informe um e-mail válido")
            self.present(alert, animated: true, completion: nil)
            
        }else if imageBool{
            let alert = alertController(with: "Selecione uma foto")
            self.present(alert, animated: true, completion: nil)
        }else{
            
            guard let email = self.emailTextField.text, let senha = self.passwordTextfield.text, let nome = self.nameTextField.text else{
            return
            }
            
            
            Auth.auth().createUser(withEmail: email, password: senha, completion: { (user, error) in
                
                if error != nil {
                    let alert = self.alertController(with: "error")
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                guard let uid = user?.uid else{
                    return
                }
                
                
                //Conta criada com sucesso
                let userReferencia = App.ref.child("users").child(uid)
                let value = ["name": nome,"email": email]
                userReferencia.updateChildValues(value, withCompletionBlock: { (error, ref) in
                    
                })
                
                
            })
//            let alert = alertController(with: "Conta Criada com sucesso")
//            self.present(alert, animated: true, completion: {
//                self.dismiss(animated: false, completion: nil)
//            })
            
            
        }
        
         self.view.endEditing(true);
    }
    override var shouldAutorotate: Bool {
        return false
    }
    
  
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true);
    }
}
extension CreateAccountViewController: UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return (true)
    }
    func handleSelectProfileImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        
        self.present(picker, animated: true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)

    }

    
    func imagePickerController(_ picker: UIImagePickerController,
                                didFinishPickingMediaWithInfo info: [String : AnyObject]) {
       if let originalName = info["UIImagePickerControllerOriginalImage"] {
            print(originalName)
        }
        
    }
}
